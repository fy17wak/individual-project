## How to load the final model

1- Download the JAFFED file 

2- Open FianlModel file on Google Collab to run on GPU

3- Change the path in the first line to the directory of JAFFED

4- Run the file 

5- Obesrve the outputs, plots and confusion matrix


## Running with the second dataset 

1- Download pictures.zip

2- Open the KDEFModel file

3- Reapeat steps 3-5 above 


Note: The KDEF model is for testing purposes only, the final model is FinalModel 
