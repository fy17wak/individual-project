import numpy as np

#generates two random matrices
matrixA = np.random.random((2,2))
matrixB = np.random.random((2,2))

#multiply the two matrices
multiplication = matrixA.dot(matrixB)

print ("matrixA = ", matrixA)
print ("matrixB = ", matrixB)
print (" matrixA * matrixB = ", multiplication)
print (" matrixA + matrixB = ")
#add the two matrices
print (matrixA + matrixB)
