import torch

#generates two random matrices
matrixA = torch.rand(2,2)
matrixB = torch.rand(2,2)

#multiplies two matricies
multiplication = torch.mm(matrixA, matrixB)


print (" matrixA = ", matrixA)
print (" matrixB = ", matrixB)
print (" matrixA * matrixB = ", multiplication)
#adds two matricies
print (" matrixA + matrixB = ", matrixA + matrixB)
